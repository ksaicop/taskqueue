﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskQueue
{
    /// <summary>
    /// Represents a queue of actions to execute.
    /// </summary>
    /// <typeparam name="T">The type of parameter of the action method</typeparam>
    public class TaskQueue<T>
    {
        #region Private Fields

        /// <summary>
        /// The action to execute for each item.
        /// </summary>
        private Action<T> action;

        /// <summary>
        /// Indicates whether queue is not empty and action is in progress.
        /// </summary>
        private bool inProgress;

        /// <summary>
        /// The queue with items to process.
        /// </summary>
        private Queue<T> queue = new Queue<T>();

        /// <summary>
        /// The sync root element.
        /// </summary>
        private object syncRoot = new object();

        /// <summary>
        /// The task currently executed.
        /// </summary>
        private Task task;

        #endregion Private Fields

        #region Public Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TaskQueue{T}"/> class.
        /// </summary>
        /// <param name="action">The action to execute on each queued item.</param>
        public TaskQueue(Action<T> action)
        {
            this.action = action;
        }

        #endregion Public Constructors

        #region Public Properties

        /// <summary>
        /// Gets a value indicating whether queue is not empty and action is in progress.
        /// </summary>
        /// <value>
        ///   <c>true</c> if queue is not empty and action is in progress; otherwise, <c>false</c>.
        /// </value>
        public bool InProgress
        {
            get
            {
                lock (this.syncRoot)
                {
                    return this.inProgress;
                }
            }
        }

        #endregion Public Properties

        #region Public Methods

        /// <summary>
        /// Adds an object to the end of the queue.
        /// </summary>
        /// <param name="item">The item.</param>
        public void Enqueue(T item)
        {
            lock (this.syncRoot)
            {
                this.queue.Enqueue(item);

                if (!this.inProgress)
                {
                    this.inProgress = true;
                    this.task = Task.Run(() => this.Process());
                }
            }
        }

        /// <summary>
        /// Waits for empty queue and last action to complete execution.
        /// </summary>
        public void Wait()
        {
            Task task = this.task;
            if (this.task != null)
            {
                this.task.Wait();
            }
        }

        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// Process all queued items.
        /// </summary>
        private void Process()
        {
            T item = this.queue.Dequeue();
            bool work = true;
            while (work)
            {
                this.action(item);
                lock (this.syncRoot)
                {
                    work = this.queue.Any();
                    if (work)
                    {
                        item = this.queue.Dequeue();
                    }
                    else
                    {
                        this.inProgress = false;
                        this.task = null;
                    }
                }
            }
        }

        #endregion Private Methods
    }
}