**TaskQueue** class was created to solve need of:
* adding items to a queue from many threads in the same time,
* run specified action for each of added item, one by one synchronously (when action for one element finish then action for second is executed),
* actions should be run asynchronously to the main thread,
* items should be processed in the order of add,
* processing actions should be run immediately after first item was added and continue until queue is empty,
* during processing of already added items, new items add is possible.