﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TaskQueue.Tests
{
    /// <summary>
    /// Tests for <see cref="TaskQueue{T}"/> class
    /// </summary>
    [TestClass]
    public class TaskQueueTests
    {
        #region Public Methods

        /// <summary>
        /// Tests with multi threads that add items in the same time to queue.
        /// </summary>
        [TestMethod]
        public void EnqueueMultiTest()
        {
            List<string> results = new List<string>();
            TaskQueue<string> queue = new TaskQueue<string>((item) =>
            {
                Thread.Sleep(100);
                results.Add(item);
                System.Diagnostics.Debug.WriteLine($"{item} processed");
            });

            int length = 10;
            Task task1 = Task.Run(() => AddItems(queue, length, "t1: "));
            Task.Run(() => AddItems(queue, length, "t2: "));
            Task.Run(() => AddItems(queue, length, "t3: "));

            while (!queue.InProgress)
            {
                Thread.Sleep(100);
            }

            queue.Wait();

            Assert.AreEqual(length * 3, results.Count);
        }

        /// <summary>
        /// Tests with one thread that adds items to queue.
        /// </summary>
        [TestMethod]
        public void EnqueueTest()
        {
            List<string> results = new List<string>();
            TaskQueue<string> queue = new TaskQueue<string>((item) =>
            {
                Thread.Sleep(100);
                results.Add(item);
                System.Diagnostics.Debug.WriteLine($"{item} processed");
            });

            int length = 10;
            AddItems(queue, length);

            queue.Wait();

            Assert.AreEqual(length, results.Count);
        }

        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// Adds the items.
        /// </summary>
        /// <param name="queue">The queue.</param>
        /// <param name="length">The length.</param>
        /// <param name="prefix">The prefix.</param>
        private static void AddItems(TaskQueue<string> queue, int length, string prefix = null)
        {
            for (int i = 0; i < length; i++)
            {
                Thread.Sleep(90);
                if (i % 3 == 0)
                {
                    Thread.Sleep(90);
                }

                string item = $"{prefix}{i}";
                queue.Enqueue(item);
                System.Diagnostics.Debug.WriteLine($"{item} enqued");
            }
        }

        #endregion Private Methods
    }
}